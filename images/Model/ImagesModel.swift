//
//  ImagesModel.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import SwiftyJSON

class ImagesModel {
    
    static var shared = ImagesModel()
    
    private init() {}
    
    /// Save image data
    /// - Parameter json: JSON SwiftyJSON object
    func save(_ json: JSON, page: Int) throws {
        for info in json.arrayValue {
            let id = info["id"].stringValue
            let user = info["user"]
            
            do {
                try ProfileModel.shared.save(user) { (data) in
                    let image = self.find(id, user: data)
                    image.populate(info)
                    image.page = page
                    image.profile = data
                    
                    image.commit()
                }
            }catch {
                print("Error al guardar datos en db")
            }
        }
    }
    
    /// Find all images
    /// - Parameter completion: [ImageSource] custom object array
    func findAll() -> [ImageSource] {
        var result = [ImageSource]()
        let data = ImageSource.query().fetch() as! [ImageSource]
        
        if data.count > 0 {
            result = data
        }
        
        return result
    }
    
    /// Find images by page
    /// - Parameters:
    ///   - page: Number page 
    /// - Returns: Array ImagesSource
    func findByPage(_ page: Int) -> [ImageSource] {
        var result = [ImageSource]()
        let data = ImageSource.query().where(withFormat: "page = %@", withParameters: [page]).fetch() as! [ImageSource]
        
        if data.count > 0 {
            result = data
        }
        
        return result
    }
    
    /// Find image by id and profile
    /// - Parameters:
    ///   - id: String
    ///   - user: ProfileSource custom db object
    func find(_ id: String, user: ProfileSource)->ImageSource {
        let data = ImageSource.query().where(withFormat: "image_id = %@ and profile", withParameters: [id, user]).fetch() as! [ImageSource]
        return data.count > 0 ? data.first! : ImageSource()
    }
    
    /// Find favorite images
    /// - Parameter completion: [ImageSource]
    func findFavorites(_ completion: @escaping ([ImageSource]) -> ()){
        var result = [ImageSource]()
        let data = ImageSource.query()?.where(withFormat: "favorite = %@", withParameters: [true]).fetch() as! [ImageSource]
        
        if data.count > 0 {
            result = data
        }
        
        completion(result)
    }
    
    /// Valid exist image data
    /// - Parameter id: String
    func exist(_ id: String, username: String)->Bool {
        let existProfile = ProfileModel.shared.exist(username)
        let data = ImageSource.query().where(withFormat: "image_id = %@", withParameters: [id]).fetch() as! [ImageSource]
        
        let result = (existProfile && data.count > 0)
        
        return result
    }
    
}
