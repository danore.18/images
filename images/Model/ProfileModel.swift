//
//  ProfileModel.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ProfileModel {
    
    static var shared = ProfileModel()
    
    private init() {}
    
    /// Save profile data
    /// - Parameters:
    ///   - json: SON SwiftyJSON object
    ///   - completion: ProfileSource custom db object
    func save(_ json: JSON, completion: @escaping (ProfileSource) -> ()) throws {
        let username = json["username"].stringValue
        
        let entity = self.find(username)
        entity.populate(json)
        entity.commit()
        
        completion(entity)
    }
    
    /// Find profile by username
    /// - Parameter username: String
    func find(_ username: String)->ProfileSource {
        let data = ProfileSource.query().where(withFormat: "username = %@", withParameters: [username]).fetch() as! [ProfileSource]
        return data.count > 0 ? data.first! : ProfileSource()
    }
    
    /// Valid exist profile data
    /// - Parameter username: String
    func exist(_ username: String)->Bool {
        let data = ProfileSource.query().where(withFormat: "username = %@", withParameters: [username]).fetch() as! [ProfileSource]
        
        return data.count > 0
    }
    
}
