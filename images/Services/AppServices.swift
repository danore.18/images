//
//  AppServices.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AppServices {
    
    static let shared = AppServices()
    
    private init(){}
    
    /// Reuest with alamofire
    /// - Parameters:
    ///   - request: RequestEntity cuscom object
    ///   - completion: (ResponseEntity) cuscom object
    func request(_ request: RequestEntity, completion: @escaping (ResponseEntity) -> ()) {
        Api.request(request) { (json, httpCode) in
            let result =  ValidResponse.shared.general(json, httpCode: httpCode)
            
            self.validTask(request, response: result) { (response) in
                completion(response)
            }
        }
    }
    
    /// Valid request task
    /// - Parameters:
    ///   - request: RequestEntity cuscom object
    ///   - response: ResponseEntity cuscom object
    private func validTask(_ request: RequestEntity, response: ResponseEntity, completion: @escaping (ResponseEntity) -> ()) {
        if response.status == .success {
            do {
                if request.type == .images {
                    var page = 0
                    
                    for (key, value) in request.params! {
                        if key.contains("page") { page = value as! Int }
                    }
                    
                    try ImagesModel.shared.save(response.json!, page: page)
                }else if request.type == .profile {
                    try ProfileModel.shared.save(response.json!) { (data) in
                        response.recordset = data as AnyObject
                    }
                }
            }catch {
                response.status = .error
                response.msg = StringUtil.ERROR_SAVE_DB
            }
        }
        
        if request.method == .get {
            if request.type == .images {
                response.status = .success
            }            
        }
        
        completion(response)
    }
    
}
