//
//  ProfileSource.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import SwiftyJSON
import SharkORM

class ProfileSource: SRKObject {
    
    @objc dynamic var user_id: String?
    @objc dynamic var name: String?
    @objc dynamic var image_profile: String?
    @objc dynamic var username: String?
    @objc dynamic var biography: String?
    @objc dynamic var twitter: String?
    @objc dynamic var instagram: String?
    @objc dynamic var total_collection: Int = 0
    @objc dynamic var total_photos: Int = 0
    @objc dynamic var total_likes: Int = 0
    
}

extension ProfileSource {
    
    /// Populate data
    /// - Parameter json: JSON SwiftyJSON Object
    func populate(_ json: JSON) {
        self.user_id = json["id"].stringValue
        self.username = json["username"].stringValue
        self.name = json["name"].stringValue
        self.image_profile = json["profile_image"]["medium"].stringValue
        self.biography = json["bio"].stringValue
        self.twitter = json["twitter_username"].stringValue
        self.instagram = json["instagram_username"].stringValue
        self.total_collection = json["total_collections"].intValue
        self.total_likes = json["total_likes"].intValue
        self.total_photos = json["total_photos"].intValue
    }
    
}
