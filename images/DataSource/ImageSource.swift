//
//  ImageSource.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import SwiftyJSON
import SharkORM

class ImageSource: SRKObject {
    
    @objc dynamic var image_id: String?
    @objc dynamic var uri: String?
    @objc dynamic var desc: String?
    @objc dynamic var likes: Int = 0
    @objc dynamic var favorite: Bool = false
    @objc dynamic var page: Int = 0
    @objc dynamic var username: String?
    @objc dynamic var profile: ProfileSource?
    
}

extension ImageSource {
    
    /// Populate data
    /// - Parameter json: JSON SwiftyJSON Object
    func populate(_ json: JSON) {
        self.image_id = json["id"].stringValue
        self.uri = json["urls"]["full"].stringValue
        self.desc = json["description"].stringValue
        self.likes = json["likes"].intValue
        self.username = json["user"]["username"].stringValue
        self.page = json["page"].intValue
        
        if json["favorite"].exists() {
            self.favorite = json["favorite"].boolValue
        }
    }
    
}
