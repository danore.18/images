//
//  ImageCell.swift
//  images
//
//  Created by Daniel Orellana on 3/6/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import UIKit
import Alamofire

protocol ImageCellDelegate : class {
    func favorite(_ tag: Int)
}

class ImageCell: UICollectionViewCell {
    weak var cellDelegate: ImageCellDelegate?

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContent.layer.cornerRadius = 8
        imgCover.layer.cornerRadius = 8
        imgProfile.layer.cornerRadius = imgProfile.bounds.height / 2
        viewContent.clipsToBounds = true
        imgCover.clipsToBounds = true
        imgProfile.clipsToBounds = true
    }
    
    @IBAction func favorite(_ sender: UIButton) {
        cellDelegate?.favorite(sender.tag)
    }
    
}
