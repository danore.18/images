//
//  MainViewController.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import UIKit

class MainViewController: UIViewController {
    
    var activityIndicator : UIActivityIndicatorView?
    
    func start() {
        if self.activityIndicator == nil {
            let activityIndicator = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.medium)
            //activityIndicator.center = loginButton.center
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            //loginButton.setTitle("", for: UIControlState.normal)
            self.activityIndicator = activityIndicator
        }
    }
    
    func stop() {
        if let activityIndicator = self.activityIndicator {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
            self.activityIndicator = nil
        }
    }
    
    func showError(_ text: String) {
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
