//
//  FavoriteViewController.swift
//  images
//
//  Created by Daniel Orellana on 3/6/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class FavoriteViewController: MainViewController, UICollectionViewDataSource, UICollectionViewDelegate, ImageCellDelegate, UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var searchBar:UISearchBar = UISearchBar()
    var list = [ImageSource]()
    var listSearch = [ImageSource]()
    var imageSource: ImageSource?
    var isSearch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialize()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        
        self.loadData()
    }
    
    /// Initialize screen
    private func initialize() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.modifiyView()
    }
    
    private func modifiyView() {
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = "Buscar..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        
        navigationItem.titleView = searchBar
                
        self.collectionView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        self.collectionView.register(UINib(nibName: "\(ImageCell.self)", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    /// Load images data
    private func loadData() {
        ImagesModel.shared.findFavorites { (data) in
            if data.count > 0 {
                self.list = data
                
                self.collectionView.reloadData()
            }else {
                self.collectionView.isHidden = true
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.isSearch ? self.listSearch.count : self.list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCell
        let entity = self.isSearch ? self.listSearch[indexPath.row] : self.list[indexPath.row]
        
        cell.lblUser.text = entity.profile?.name?.capitalized
        cell.lblLike.text = "\(entity.likes) Likes"
        
        cell.imgProfile.sd_setImage(with: URL(string: entity.profile!.image_profile!), placeholderImage: UIImage(named: "ic_profile_default"))
        
        let origImage = UIImage(systemName: entity.favorite ? "suit.heart.fill" : "suit.heart")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            cell.btnFavorite.setImage(tintedImage, for: .normal)
        cell.btnFavorite.tintColor = entity.favorite ? .red : .darkGray
        
        cell.imgCover.sd_setImage(with: URL(string: entity.uri!), placeholderImage: UIImage(named: "ic_not_content"))
        
        cell.btnFavorite.tag = indexPath.row
        cell.cellDelegate = self
        
        return cell
    }
    
    func collectionView(_ colectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let entity = self.list[indexPath.row]
        self.imageSource = entity
        
        performSegue(withIdentifier: SegueUtil.SG_DETAIL, sender: self)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
        self.isSearch = textSearched.isEmpty ? false : true
        self.listSearch = self.list.all(where: { $0.profile!.name!.contains(textSearched) || $0.username!.contains(textSearched) })
        
        self.collectionView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    func favorite(_ tag: Int) {
        let entity = self.list[tag]
        entity.favorite.toggle()
        entity.commit()
        
        let indexPath = IndexPath(row: tag, section: 0)
        
        self.list.remove(at: tag)
        self.collectionView.deleteItems(at: [indexPath])
        
        self.collectionView.reloadData()
    }
    
    /// Load images
    ///
    /// - Parameters:
    ///   - image: UIImageView
    ///   - url: String
    func loadImage(_ image: UIImageView, url: String){
        if(url.count > 0){
            let urlData = URL(string: url)
            image.af_setImage(withURL: urlData!)
        }else{
            image.image = UIImage(named: "ic_not_content")
        }
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destViewController = segue.destination as! UINavigationController
        
        if (segue.identifier!.elementsEqual(SegueUtil.SG_DETAIL)) {
            if let destViewController : ImageDetailViewController = destViewController.topViewController as? ImageDetailViewController {
                destViewController.profileSource = self.imageSource?.profile
            }
        }
    }
    
}
