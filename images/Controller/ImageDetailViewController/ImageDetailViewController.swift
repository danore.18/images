//
//  ImageDetailViewController.swift
//  images
//
//  Created by Daniel Orellana on 3/7/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import UIKit

class ImageDetailViewController: MainViewController {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblImages: UILabel!
    @IBOutlet weak var lblCollections: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var lblTwitter: UILabel!
    @IBOutlet weak var lblInstagram: UILabel!
    @IBOutlet weak var viewTw: UIView!
    @IBOutlet var viewIns: UIView!
    @IBOutlet weak var nsConstraintTw: NSLayoutConstraint!
    
    var profileSource: ProfileSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialize()
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /// Initialize screen
    private func initialize() {
        self.modifyView()
        self.populateData()
    }
    
    /// Modify view elements
    private func modifyView() {
        self.imgProfile.layer.cornerRadius = self.imgProfile.bounds.height / 2
        self.imgProfile.clipsToBounds = true
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    /// Populate detail data
    private func populateData() {
        self.loadImage(self.imgProfile, url: (self.profileSource!.image_profile)!)
        self.lblName.text = self.profileSource?.name?.capitalized
        self.lblDesc.text = self.profileSource?.biography?.capitalized
        self.lblImages.text = self.profileSource?.total_photos.toString
        self.lblCollections.text = self.profileSource?.total_collection.toString
        self.lblLikes.text = self.profileSource?.total_likes.toString
        self.lblTwitter.text = "@\(self.profileSource!.twitter!)"
        self.lblInstagram.text = "@\(self.profileSource!.instagram!)"
        
        if (self.profileSource?.twitter!.isEmpty)! {
            self.viewTw.isHidden = true
            self.nsConstraintTw.constant = 0
        }else if (self.profileSource?.instagram!.isEmpty)! {
            self.viewIns.isHidden = true
        }
    }
    
    /// Load images
    ///
    /// - Parameters:
    ///   - image: UIImageView
    ///   - url: String
    func loadImage(_ image: UIImageView, url: String){
        let urlData = URL(string: url)
        image.af_setImage(withURL: urlData!)
    }
    
}
