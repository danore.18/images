//
//  ResponseEntity.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ResponseEntity {
    
    var status: HttpStatus?
    var code: Int?
    var msg: String?
    var json: JSON?
    var recordset: AnyObject?
    
}
