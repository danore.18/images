//
//  RequestEntity.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire

class RequestEntity {
    
    var params: Parameters?
    var method: HTTPMethod = .get
    var type: RequestType = .images
    var uri: String?
    
}
