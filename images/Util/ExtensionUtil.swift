//
//  ExtensionUtil.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire

let imageCache = NSCache<AnyObject, AnyObject>()

extension String {
    /// Replace string
    /// - Parameters:
    ///   - params: Parameters Alamofire
    func replace(_ params: Parameters) ->String {
        var value = self
        
        for param in params{
            value = value.replacingOccurrences(of: "{\(param.key)}", with: "\(param.value)")
        }
        
        return value
    }
}

extension UIImageView {
    
    func load(_ urlString: String) {
        if let image = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = image
            return
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        imageCache.setObject(image, forKey: urlString as NSString)
                        self?.image = image
                    }
                }
            }
        }
    }
    
}

extension Int {
    
    /// Parse int to string
    var toString: String {
        return String(self)
    }
    
}

extension Array where Element: Equatable {
    func all(where predicate: (Element) -> Bool) -> [Element]  {
        return self.compactMap { predicate($0) ? $0 : nil }
    }
}
