//
//  StringUtil.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation

class StringUtil {
    
    static let SUCCESS = "Solicitud realizada satisfactoriamente"
    static let NOT_CONTENT = "No existen resultados"
    static let BAD_REQUEST = "Verificar que los datos enviados sean correctos"
    static let AUTHORIZED = "No tienes autorización para realizar consulta"
    static let NOT_FOUND = "Recurso no encontrado"
    static let NOT_ALLOWED = "Problemas al conectar con este recurso"
    static let CRITICAL_ERROR = "Algo salió mal, intenta más tarde"
    static let SERVER_ERROR = "Problemas con el servidor, intenta más tarde"
    static let READ_CARD_ERROR = "Error al leer datos de tarjeta"
    static let NOT_CONTENT_FIELD = "Campo requerido"
    
    // String error
    static let ERROR_SAVE_DB = "Error al guardar registor en base de datos"
    
}
