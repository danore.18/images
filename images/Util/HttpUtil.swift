//
//  HttpUtil.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation

enum HttpCode: Int {
    
    case success
    case created
    case not_content
    case bad_request
    case authorized
    case forbidden
    case not_found
    case method_not_allowed
    case server_error
    
}

enum HttpStatus {
    
    case success
    case error
    
}

class HttpUtil {
    
    init(){
    }
    
    /// Get http code
    ///
    /// - Parameter httpCode: Int http code
    /// - Returns:  HttpCode custon class
    static func code(_ httpCode: Int) ->HttpCode {
        switch httpCode {
        case 200:
            return HttpCode.success
        case 201:
            return HttpCode.created
        case 204:
            return HttpCode.not_content
        case 400:
            return HttpCode.bad_request
        case 401:
            return HttpCode.authorized
        case 403:
            return HttpCode.forbidden
        case 404:
            return HttpCode.not_found
        case 405:
            return HttpCode.method_not_allowed
        default:
            return HttpCode.server_error
        }
    }
    
}

enum RequestType {
    
    case images
    case profile
    
}
