//
//  Routes.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation

class Routes {
    
    static var shared = Routes()
    
    private init() {}
    
    /// Uri images data
    var images: String {
        return "\(SettingsUtil.API_URI)\(Path.IMAGES_PATH)"
    }
    
}
