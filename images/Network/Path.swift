//
//  Path.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation

class Path {
    
    static let IMAGES_PATH = "/photos"
    static let PROFILE_PATH = "/users/{username}"
    
}
