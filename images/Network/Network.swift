//
//  Network.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Network {
    
    static let shared = Network()
    
    private init(){}
    
    /// Get params header
      private func getHeaders() ->HTTPHeaders?{
          return [:]
      }
    
    /// Http Request
    /// - Parameters:
    ///   - url: String
    ///   - params: Parameters Alamofire
    ///   - httpMethod: HTTPMethod Alamofire
    ///   - completionHandler: (JSON, HttpCode)
    func request(_ url: String, params: Parameters? = nil, httpMethod: HTTPMethod, completionHandler: @escaping (JSON, HttpCode) -> ()) {
        let manager = Alamofire.Session.default
        manager.session.configuration.timeoutIntervalForRequest = 15
        var request: DataRequest
        
        if(Reachability.shared.isConnectedToNetwork){
            if httpMethod == .get {
                if params != nil {
                    request = AF.request(url, method: .get, parameters: params, headers: self.getHeaders())
                }else {
                    request = AF.request(url, method: .get, encoding: JSONEncoding.default, headers: self.getHeaders())
                }
            }else if httpMethod == .post || httpMethod == .put {
                request = AF.request(url, method: httpMethod, parameters: params, encoding: JSONEncoding.default, headers: self.getHeaders())
            }else {
                request = AF.request(url, method: .delete, parameters: params, headers: self.getHeaders())
            }
            
            request.responseJSON { response in
                    self.validResponse(response) { (json, httpCode) in
                        completionHandler(json, httpCode)
                    }
            }
        }else{
            completionHandler(JSON.null, .server_error)
        }
    }
    
    /// Valid response data
    /// - Parameters:
    ///   - response: DataResponse<Any>
    ///   - completion: (JSON, HttpCode)
    private func validResponse(_ response: DataResponse<Any>, completion: @escaping (JSON, HttpCode) -> ()) {
        switch response.result {
        case .success(_):
            let httpCode = HttpUtil.code((response.response?.statusCode)!)
            
            if(httpCode == .success){
                let jsonData = try! JSON(data: response.data!)
                
                completion(jsonData, httpCode)
            }else{
                completion(JSON.null, httpCode)
            }
        case .failure(_):
            if(response.response != nil){
                let httpCode = HttpUtil.code((response.response?.statusCode)!)
                
                if(httpCode == .authorized){
                    completion(JSON.null, httpCode)
                }else{
                    completion(JSON.null, httpCode)
                }
            }else{
                completion(JSON.null, .server_error)
            }
        }
    }
    
}
