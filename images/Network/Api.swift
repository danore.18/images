//
//  Api.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Api {
    
    /// Conection request
    /// - Parameters:
    ///   - request: RequestEntity custom object request data
    ///   - completionHandler: (JSON, HttpCode) custom return
    static func request(_ request: RequestEntity, completionHandler: @escaping (JSON, HttpCode) -> ()) {
        Network.shared.request(request.uri!, params: request.params, httpMethod: request.method, completionHandler: completionHandler)
    }
    
}
