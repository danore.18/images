//
//  ValidResponse.swift
//  images
//
//  Created by Daniel Orellana on 2/26/20.
//  Copyright © 2020 Daniel Orellana. All rights reserved.
//

import Foundation
import SwiftyJSON

class ValidResponse {
    
    static var shared = ValidResponse()
    
    private init() {}
    
    /// Serialize general response
    /// - Parameters:
    ///   - json: JSON SwiftyJSON object
    ///   - httpCode: HttpCode custom object http
    func general(_ json: JSON, httpCode: HttpCode) ->ResponseEntity {
        let response = ResponseEntity()
        
        if(httpCode != HttpCode.server_error){
            response.code = httpCode.hashValue
            
            if(httpCode == HttpCode.success || httpCode == HttpCode.created){
                response.status = .success
                response.msg = StringUtil.SUCCESS
                response.json = json
            }else if(httpCode == .not_content){
                response.msg = StringUtil.NOT_CONTENT
            }else if(httpCode == .bad_request){
                response.msg = StringUtil.BAD_REQUEST
            }else if(httpCode == .authorized){
                response.msg = StringUtil.AUTHORIZED
            }else if(httpCode == .forbidden){
                response.status = .error
                response.msg = StringUtil.AUTHORIZED
            }else if(httpCode == .not_found){
                response.status = .error
                response.msg = StringUtil.NOT_FOUND
            }else if(httpCode == HttpCode.method_not_allowed){
                response.status = .error
                response.msg = StringUtil.NOT_ALLOWED
            }else if(httpCode == .server_error){
                response.status = .error
                response.msg = StringUtil.SERVER_ERROR
            }else{
                response.status = .error
                response.msg = StringUtil.CRITICAL_ERROR
            }
        }else{
            response.code = 500
            response.status = .error
            response.msg = StringUtil.CRITICAL_ERROR
        }
        
        return response
    }
    
}
